class AddIndexToUsersEmail < ActiveRecord::Migration[5.0]
  def change
  	add_index :users, :email, unique:true #fazendo com que o email seja unico no banco de dados
  end
end
