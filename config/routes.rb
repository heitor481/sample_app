Rails.application.routes.draw do

	#help_path -> '/help' pega pelo nome da rota usando o #
	#help_url  -> 'http://www.example.com/help' pega pela url usando o /
	#o as: irá definir uma rota com o nome que você quiser as: 'helf'
  
  root 'static_pages#home'

  get '/help', to: 'static_pages#help'#named routes

  get '/about', to: 'static_pages#about' #named routes

  get '/contact', to: 'static_pages#contact' #named routes

  get '/signup', to: 'users#new'

  post '/signup', to: 'users#create'

  #get 'sessions/new'

  get '/login', to: 'sessions#new'

  post '/login', to: 'sessions#create'

  delete '/logout', to: 'sessions#destroy'

  resources :users
end
