class SessionsController < ApplicationController
  def new
  end

  #log_in é o metodo do rails para fazer o login do usuario
  #o redirect_to esta convertendo users_url(user) para a rota do usuario
  def create
  	user = User.find_by(email: params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
  		log_in user
  		redirect_to user
  	else
  		flash.now[:danger] = 'Invalidos usuario/email'
  		render 'new'
  	end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
