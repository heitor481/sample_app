class UsersController < ApplicationController
  def new
  	@user = User.new
  	#debugger
  end

  def show
  	@user = User.find(params[:id])
  	#debugger ele irá fazer um debugg do projeto 
  end

  def create
  	@user = User.create(users_params)
  	if @user.save
      log_in @user
  		flash[:success] = "Welcome to sample app"
  		redirect_to @user
  	else
  		render 'new'
  	end
  end	

  private

  #como não vai ficar acessível aos usuarios, esse metodo ficara como privado acessivel só dentro da classe
  #estamos basicamente usando o strong parameters para passar os parametros necessário para a criação do usuario
  def users_params
  	params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  
end
