module SessionsHelper
	#log in de usuarios
	#transforma em hash o :user_id e grava esse cookie no browser do cliente
	def log_in(user)
		session[:user_id] = user.id 
	end


	#irá retornar o usuario logado no sistema, se não tiver nenhum, retornar falso
	def current_user
		@current_user ||= User.find_by(id: session[:user_id]) 
	end


	def logged_in?
		!current_user.nil?
	end


	def log_out
		session.delete(:user_id)
		@current_user = nil
	end
end
