class User < ApplicationRecord
	#regex para verificar se o email é válido
	#o uniqueness garante que o email seja único para todos, mas não garante na base de dados
	#o before é um callback que irá fazer com que os emails sejam salvos como minusculas no banco
	#o has_secure_password garante que a senha seja salva no formato de hash (criptografia) 
	before_save { self.email = email.downcase }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
	validates :name, presence:true, length: { maximum: 50 }
	validates :email, presence:true, length: { maximum: 255 }, format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}

	has_secure_password
	validates :password, presence:true, length:{ minimum: 6 }


	def User.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
	end
	
end
