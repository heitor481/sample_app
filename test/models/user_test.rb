require 'test_helper'

class UserTest < ActiveSupport::TestCase
  #o teste da vermelho pq o @user cria uma nova variavel de instancia sem o password e a confirmação
  def setup
  	@user = User.new(name: "Heitor Ribeiro", email: "heitor_481@hotmail.com", password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid?" do
  	assert @user.valid?
  end

  test "name should be present?" do
  	@user.name = ""
  	assert_not @user.valid?
  end

  test "email should be present?" do
  	@user.email = ""
  	assert_not @user.valid?
  end

  test "name should not be too long" do
  	@user.name = "a" * 51
  	assert_not @user.valid?
  end

  test "email should not be too long" do
  	@user.email = "a" * 244 + "@example.com"
  	assert_not @user.valid?
  end

  test "email validation should accept only valid adresses" do
  	invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email adress should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid? 
  end


  test "password should not be blank" do
    @user.password = @user.password_confirmation = "" * 6
    assert_not @user.valid? 
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end


end