require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

#quando adicionado na rota o caminho, o rails gera automaticamente esse helper static_pages_about_url
#usando as named routes para fazer as rotas com nomes
#Se for usar por url use o help_url ou help_path

  test "should get root" do
    get root_path
    assert_response :success
    assert_select "title", "Ruby on Rails Tutorial Sample App"
  end

  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Help | Ruby on Rails Tutorial Sample App" #interpolação de variavel
  end

  test "should get about" do
  	get about_path 
  	assert_response :success
  	assert_select "title", "About | Ruby on Rails Tutorial Sample App" #verifica se existe a presença da tag <title>
  end

  test "should get contact" do
  	get contact_path
  	assert_response :success
  	assert_select "title", "Contact | Ruby on Rails Tutorial Sample App"
  end
end
